# 30per5

A small game in python/pygame

## dependancies:

pygame

## how to run:

python 30per5.py

## how to play:

Colorize areas such that you have same-color areas of 5 squares for which the
sum is 30.  
The black square are just empty.  
Choose the color by clicking on the right colored boxes.  
Left-click will colorize with the chosen color.  
Right-click will erase the color.  
Clicking on "s" will show a solution.  
Clicking on "r" will create a brand new grid.  
