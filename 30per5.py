#!/usr/bin/env python

#    This file is part of 30per5.
#
#    TWIM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TWIM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with 30per5.  If not, see <http://www.gnu.org/licenses/>.

#https://gitlab.com/cauch/30per5

try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.1 or higher is required")

from random import *
from math import *
import os

class Square(pygame.sprite.Sprite):

    SIZE = 48

    def __init__(self,interface,pos,role):
        """
        sprite corresponding to a square
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.size = Square.SIZE
        self.interface = interface #the Interface object
        self.pos = pos             #position, in pixel
        self.role = role           #role
        #role can be: "gX,Y" a square of the grid, corresponding to X,Y grid coord
        #             "cX" a button to change the current color to the color number X
        #             "nX" just to display stuff, by default, the text is "X"
        self.nr = 1      #need refresh
        self.text = ""   #extra attribute for when needed
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)),"data/font/Handlee-Regular.ttf")
        self.font = pygame.font.Font(filename, 50)
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        self.refresh()

    def refresh(self):
        """
        called by the main display loop
        """
        if self.nr == 0:
            return 0
        self.nr = 0
        self.image.fill([0,0,0])

        text = ""
        cl = 0
        #if "g", get the text and color
        if self.role.startswith("g"):
            x,y = [int(v) for v in self.role[1:].split(",")]
            text = self.interface.grid.Get_number(x,y)
            cl = self.interface.grid.Get_color(x,y)
            if cl >= len(self.interface.color):
                cl += 1-len(self.interface.color)
                if cl >= len(self.interface.color):
                    cl = (cl%(len(self.interface.color)-1))+1
        #if "c"
        if self.role.startswith("c"):
            cl = int(self.role[1:])
            text = ""
            if self.interface.sel_color == cl:
                text = "X"
        #if "n"
        if self.role.startswith("n"):
            if self.text != "":
                text = self.text
            else:
                text = self.role[1:]

        self.text = text
        if text == "":
            self.image.fill([8,8,8])
        if cl != 0:
            loi = self.interface.filled_images
            self.image.blit( loi["%i_4"%(cl)], (0,0) )
        font = self.font
        cl1 = (0,0,0,100)
        if cl == 0:
            cl1 = (255,255,255,100)
        textImg1 = font.render( text, 1, cl1)
        textImg2 = font.render( text, 1, (155,155,155,100))
        w = textImg1.get_width()+5
        h = textImg1.get_height()+5
        bkgd = pygame.Surface((w,h)).convert_alpha()
        bkgd.fill((0,0,0,1))
        bkgd.blit( textImg2, (1,1) )
        bkgd.blit( textImg2, (3,3) )
        bkgd.blit( textImg1, (2,2) )
        factor = self.size*1./h
        bkgd2 = pygame.transform.smoothscale(bkgd, (int(w*factor), int(h*factor)))
        dx = (self.size-int(w*factor))/2
        self.image.blit( bkgd2, (dx,0))
        return 1

    def Click(self,button):
        if self.role.startswith("g"):
            if self.text == "":
                return
            rx,ry = [int(v) for v in self.role[1:].split(",")]
            if button[0] == 1:
                self.interface.click_on_grid(rx,ry)
                self.nr = 1
            if button[2] == 1:
                self.interface.unclick_on_grid(rx,ry)                
                self.nr = 1
        if self.role.startswith("c"):
            i = int(self.role[1:])
            self.interface.click_color(i)
        if self.role == "n<":
            self.interface.moveview(0)
        if self.role == "n^":
            self.interface.moveview(1)


    #def Change(self,cl=None,text=None):
    #    if text != None:
    #        self.text = text
    #        self.nr = 1
    #    if cl != None:
    #        self.cl = cl
    #        self.nr = 1


class Grid:
    def __init__(self,size,area,total):
        self.g1 = []
        self.g2 = []
        self.o = []
        self.solution = []
        self.win = []
        self.size = size
        self.area = area
        self.total = total

    def Generate(self):
        for i in range(1000):
            s = self.Generate2()
            if s:
                break

    def Generate2(self):
        self.g1 = []
        self.g2 = []
        for i in range(self.size**2):
            ii = 0
            if randint(0,8)==0:
                ii=1
            self.g1.append(ii)
            self.g2.append(0)

        clnum = 2
        i = randint(0,self.size**2)
        dw = 0
        while dw < self.size**2:
            s = self.Create_island(i,clnum)
            i += 1
            if i >= self.size*self.size:
                i = 0
            if s == 1:
                clnum +=1
                dw = 0
            else:
                dw += 1
        
        if self.g1.count(0) > 16 or self.g1.count(0) < 10:
            return 0
        else:
            x = max(self.g1)
            lon = {}
            for i in range(1,x+1):
                #find area number that sum to self.total
                lon[i] = self.Find_sum()
            for i in range(self.size**2):
                index = self.g1[i]
                if index == 0 or index == 1:
                    self.g2[i] = ""
                else:
                    self.g2[i] = str(lon[index][0])
                    lon[index] = lon[index][1:]
        self.solution = []
        for i in self.g1:
            if i > 1:
                self.solution.append(i-1)
            else:
                self.solution.append(0)
        self.win = []
        for i in self.g2:
            if i == "":
                self.win.append(1)
            else:
                self.win.append(0)
        self.g1 = []
        for i in range(self.size**2):
            self.g1.append(0)                
        return 1

    def Find_sum(self):
        l = []
        for i in range(self.area-1):
            l.append(randint(1,self.total/2))
        if sum(l) < self.total-1:
            l.append(self.total-sum(l))
            shuffle(l)
            return l
        else:
            return self.Find_sum()

    def Move_random(self,i,ii=-1):
        if ii == -1:
            ii = randint(0,3)
        j = 0
        rl,rr = [],[]
        for x in range(self.size):
            rl.append(x*self.size)
            rr.append(self.size-1+x*self.size)
        if ii == 0:
            if i not in rl:
                j = i-1
            else:
                j = i+self.size-1
        elif ii == 1:
            if i not in rr:
                j = i+1
            else:
                j = i-self.size+1
        elif ii == 2:
            j = i-self.size
        elif ii == 3:
            j = i+self.size
        while j < 0: j+= (self.size**2)
        while j >= self.size**2: j-= (self.size**2)
        return j

    def Create_island(self,i,clnum):
        if self.g1[i] != 0:
            return 0
        self.g1[i] = clnum
        vi = [i]
        dw1 = 0
        while len(vi)<self.area and dw1<20:
            dw1 += 1
            i = vi[randint(0,len(vi)-1)]
            j = i
            dw2 = 0
            while self.g1[j] != 0 and dw2<10:
                dw2 += 1
                j = self.Move_random(i)
            if self.g1[j] == 0:
                self.g1[j] = clnum
                vi.append(j)
        if dw1 < 20:
            return 1
        else:
            for j in vi:
                self.g1[j] = 0
            return 0

    def Get_color(self,x,y):
        i = (y*self.size)+x
        return self.g1[i]

    def Get_number(self,x,y):
        i = (y*self.size)+x
        return str(self.g2[i])

    def Set_object(self,o,x,y):
        i = (y*self.size)+x
        while len(self.o) <= i:
            self.o.append(None)
        self.o[i] = o

    def Change(self,sel_color,x,y):
        i = (y*self.size)+x
        self.g1[i] = sel_color

    def Sum(self,x,y,rem=0):
        i = (y*self.size)+x
        ltt = [i]
        lse = []
        cl = self.g1[i]
        if cl == 0:
            return 0,0,0
        while ltt:
            nltt = []
            for ii in ltt:
                if self.g1[ii] != cl:
                    continue
                if ii not in lse:
                    lse.append(ii)
                iis = [self.Move_random(ii,0),self.Move_random(ii,1),
                       self.Move_random(ii,2),self.Move_random(ii,3)]
                for iiss in iis:
                    if iiss in lse+ltt+nltt:
                        continue
                    nltt.append(iiss)
            ltt = nltt[:]
        su,nu = 0,0
        for j in lse:
            if self.g2[j]:
                su += int(self.g2[j])
        nu = len(lse)
        if rem:
            su -= int(self.g2[i])
            nu = len(lse)-1
        if nu == self.area and su == self.total:
            for j in lse:
                self.win[j] = 1
        else:
            for j in lse:
                self.win[j] = 0
        #print "win:"
        #for i in range(self.size*self.size):
        #    if i%self.size == 0:
        #        print ""
        #    print self.win[i],
        #print ""
        win = 0
        if self.win.count(0) == 0:
            win = 1
        return su,nu,win


class Interface:
    def __init__(self):        
        self.screen_width = 720
        self.screen_height = 520
        self.screen_winstyle = 0
        self.screen = None
        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn
        self.clock = None
        self.play = 1
        self.time = 0
        self.win = 0

        self.pgo = [] #objects
        self.pgs = None #sprite

        self.key_pressed = [] #list of keys currently pressed
        self.mouse_pressed = None #for the mouse

        self.cards_images = {} #Surface images for cards

    def init(self):
        """
        Initiallize everything needed for pygame
        """
        if not pygame.image.get_extended():
            raise SystemExit, "Sorry, extended image module required"

        pygame.init()
        if pygame.mixer and not pygame.mixer.get_init():
            print 'Warning, no sound'
            pygame.mixer = None

        pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 16)
        pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 1)
        pygame.display.gl_set_attribute(pygame.GL_ALPHA_SIZE, 8)
        r = (self.screen_width, self.screen_height)
        bestdepth = pygame.display.mode_ok(r, self.screen_winstyle, 32)
        self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF, bestdepth)

        self.clock = pygame.time.Clock()

        #self.set_background()
        self.bkgd = pygame.Surface( r )
        self.bkgd.fill((255,0,0))
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data", "px", "bkgd.png")
        try:
            bg = pygame.image.load(filename)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())
        for x in range(1+int(self.screen_width/bg.get_width())):
            for y in range(1+int(self.screen_height/bg.get_height())):
                self.bkgd.blit(bg, (x*bg.get_width(),y*bg.get_height()))
        self.screen.blit(self.bkgd, (0,0))
        pygame.display.update()
        
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)),"data","px","icon.png")
        try:
            icon = pygame.image.load(filename)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())
        pygame.display.set_icon(icon)
        pygame.display.set_caption('30per5')
        pygame.mouse.set_visible(1)

        #no need for LayeredUpdate
        self.pgs = pygame.sprite.RenderUpdates()
        Square.containers = self.pgs

        self.color = self.Create_color()
        self.color += self.Create_color()[:-1]
        self.color = [[0,0,0]]+self.color

        self.filled_images = {}
        self.Load_images()

        self.grid_size = 10
        self.grid_area = 5
        self.grid_total = 30
        self.Create_grid()
        self.sel_color = 0
        self.button_color = []
        self.button_status1 = None
        self.button_status2 = None
        self.button_dir1 = None
        self.button_dir2 = None
        self.Create_button()

    def Load_images(self):
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data","px","i04.png")
        i1 = None
        try:
            i1 = pygame.image.load(filename)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())
        for i in range(len(self.color)-1):
            piece = i1.copy().convert_alpha()
            l = self.color[i+1]
            arr = pygame.surfarray.pixels3d(piece)
            arr[:,:,0] = arr[:,:,0]+l[0]-255
            arr[:,:,1] = arr[:,:,1]+l[1]-255
            arr[:,:,2] = arr[:,:,2]+l[2]-255
            del arr
            self.filled_images["%i_4"%(i+1)] = piece

    def Create_button(self):
        w = Square.SIZE
        self.square_size = w
        for i in range(len(self.color)):
            self.pgo.append(Square(self,[600+((i%2)*(w+2)),10+((w+2)*(i/2))],"c%i"%i))
            self.button_color.append(self.pgo[-1])
        self.pgo.append(Square(self,[600,10+w+2+((w+2)*(len(self.color)/2))],"n0"))
        self.button_status1 = self.pgo[-1]
        self.pgo.append(Square(self,[600+w+2,10+w+2+((w+2)*(len(self.color)/2))],"n0"))
        self.button_status2 = self.pgo[-1]
        #self.pgo.append(Square(self,[600,10+w+2+((w+2)*(2+len(self.color)/2))],"n<"))
        #self.button_dir1 = self.pgo[-1]
        #self.pgo.append(Square(self,[600+w+2,10+w+2+((w+2)*(2+len(self.color)/2))],"n^"))
        #self.button_dir2 = self.pgo[-1]

    def Create_grid(self):
        self.grid = Grid(self.grid_size,self.grid_area,self.grid_total)
        self.grid.Generate()
        w = Square.SIZE
        for y in range(self.grid_size):
            for x in range(self.grid_size):
                self.pgo.append(Square(self,[10+((w+2)*x),10+((w+2)*y)],"g%i,%i"%(x,y)))
                self.grid.Set_object(self.pgo[-1],x,y)

    def Create_color(self):
        colors = []
        wd = 50
        while len(colors) < 6 and wd > 0:
            norm = randint(350,500)
            r = [randint(50,250),randint(50,250),randint(50,250)]
            rf = norm*1./(r[0]+r[1]+r[2])
            r = [int(r[0]*rf),int(r[1]*rf),int(r[2]*rf)]
            if max(r) > 255:
                continue
            ok = 1
            for l in colors:
                dr = sqrt((l[0]-r[0])**2 + (l[1]-r[1])**2 + (l[2]-r[2])**2)
                if dr < 100:
                    ok = 0
            if ok:
                colors.append(r[:])
            else:
                wd += -1
        if wd <= 0:
            return self.Create_color()
        return colors


    def check_key(self):
        """
        keyboard events
        """
        for key in self.key_pressed:
            if key == K_s:
                if (self.grid.g1 != self.grid.solution[:]):
                    self.grid_old = self.grid.g1[:]
                    self.grid.g1 = self.grid.solution[:]
                else:
                    self.grid.g1 = self.grid_old[:]
                #print self.grid.g1 
                for u in range(self.grid_size**2):
                    self.grid.o[u].nr = 1
            if key == K_r:
                self.grid.Generate()
                for u in range(self.grid_size**2):
                    self.grid.o[u].nr = 1

    def moveview(self,left):
        w = Square.SIZE
        vmin = 10
        vmax = 10+((w+2)*(self.grid_size-1))
        dv = w+2
        for o in self.pgo:
            if not o.role.startswith("g"):
                continue
            if left == 0:
                o.rect.left += dv
                if o.rect.left > vmax:
                    o.rect.left += (-1*dv)
                    o.rect.left += (-1*vmax)+vmin
            else:
                o.rect.top += dv
                if o.rect.top > vmax:
                    o.rect.top += (-1*dv)
                    o.rect.top += (-1*vmax)+vmin

    def click_on_grid(self,rx,ry):
        su,ne,win = self.grid.Sum(rx,ry,1)
        s = self.grid_size
        i = rx+(ry*s)
        self.grid.Change(self.sel_color,rx,ry)
        self.grid.o[i].nr = 1
        if self.sel_color != 0:
            su,ne,win = self.grid.Sum(rx,ry,0)
            self.button_status1.text = str(su)
            self.button_status2.text = str(ne)
            self.button_status1.nr = 1
            self.button_status2.nr = 1
            if win:
                self.win = 1
                print "win!!"

    def unclick_on_grid(self,rx,ry):
        su,ne,win = self.grid.Sum(rx,ry,1)
        s = self.grid_size
        i = rx+(ry*s)
        if self.sel_color != 0:
            su,ne,win = self.grid.Sum(rx,ry,1)
            self.button_status1.text = str(su)
            self.button_status2.text = str(ne)
            self.button_status1.nr = 1
            self.button_status2.nr = 1
        self.grid.o[i].nr = 1
        self.grid.Change(0,rx,ry)

    def click_color(self,i):
        self.button_color[self.sel_color].nr = 1
        self.sel_color = i
        self.button_color[self.sel_color].nr = 1
        self.button_status1.text = "0"
        self.button_status2.text = "0"
        self.button_status1.nr = 1
        self.button_status2.nr = 1

    def mousepress(self,button,xy):
        """
        mouse events
        """
        x,y = xy[0],xy[1]
        selected = None
        for box in self.pgo:
            if box.rect.collidepoint(x,y): 
                selected = box
                break
        
        if selected:
            selected.Click(button)

    def check_events(self):
        """
        Check events
        """
        for event in pygame.event.get():
            if event.type == QUIT:
                #quit
                self.play = 0
            elif event.type == KEYDOWN:
                #keydown: add it to key_pressed
                if event.key not in self.key_pressed:
                    self.key_pressed.append(event.key)
            elif event.type == KEYUP:
                #keyup: remove it to key_pressed
                if event.key in self.key_pressed:
                    self.key_pressed.remove(event.key)
            elif event.type == MOUSEBUTTONDOWN:
                #mouse click
                self.mouse_pressed = [pygame.mouse.get_pressed(),pygame.mouse.get_pos(),[0,0]]
            elif event.type == MOUSEMOTION:
                if (event.buttons[0]):
                    dx = self.mouse_pressed[1][0]-pygame.mouse.get_pos()[0]+(self.square_size/2)
                    dy = self.mouse_pressed[1][1]-pygame.mouse.get_pos()[1]+(self.square_size/2)
                    dx = int(dx/self.square_size)
                    dy = int(dy/self.square_size)
                    r1 = [dx,dy]
                    r2 = self.mouse_pressed[2]
                    if r1[0] != r2[0]:
                        d = (r2[0]-r1[0])
                        if d < 0:
                            d += self.grid.size
                        for i in range(d):
                            self.moveview(0)
                    if r1[1] != r2[1]:
                        d = (r2[1]-r1[1])
                        if d < 0:
                            d += self.grid.size
                        for i in range(d):
                            self.moveview(1)
                    self.mouse_pressed[2] = r1
            elif event.type == MOUSEBUTTONUP:
                dx = self.mouse_pressed[1][0]-pygame.mouse.get_pos()[0]
                dy = self.mouse_pressed[1][1]-pygame.mouse.get_pos()[1]
                if dx**2 + dy**2 < 1000:
                    self.mousepress(self.mouse_pressed[0],self.mouse_pressed[1])
        #look which keys are pressed in key_pressed
        self.check_key()

    def update(self):
        """
        Update everything to be updated on the screen
        Used in the main loop
        """
        #mouse and key event
        self.check_events()

        for u in self.pgo:
            s = u.refresh()    
            #update
            if s:
                self.pgs.clear(self.screen, self.bkgd)
        dirty = []
        dirty.append(self.pgs.draw(self.screen))

        self.time += 1
        if self.win:
            if self.grid.win.count(0) != 0:
                self.win = 0
            if self.time % 15 == 0 and self.win:
                shift = randint(1,10)
                for u in range(len(self.grid.g1)):
                    cl = self.grid.g1[u]
                    if cl != 0:
                        self.grid.g1[u] += shift
                for u in range(self.grid_size**2):
                    self.grid.o[u].nr = 1

        for l in dirty:
            pygame.display.update(l)
        self.clock.tick(40)

if __name__ == '__main__':
    i = Interface()
    i.init()
    while i.play:
        i.update()
    
